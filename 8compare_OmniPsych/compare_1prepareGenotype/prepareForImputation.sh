##############################################################################################
## 1. Converts the ASCII version of PLINK (.ped, .map) to binary version (.bed, .bim, fam). ##
##    This compresses from 11GB to 736MB and faster to load in PLINK                        ##
##############################################################################################

cd /data/genotypes/GUSTO_Omni1M.PsychArray/1genotyped_PLINK

plink --file /home/adai/NAS/DATA/All_Omni_PsychChip_SNPdata_merge/PLINK/Mother/mother_merge_PLINK --make-bed --out mothers
plink --file /home/adai/NAS/DATA/All_Omni_PsychChip_SNPdata_merge/PLINK/Infant/infant_merge       --make-bed --out infants

wc -l *.fam
#  978 infants.fam
#  955 mothers.fam
# 1933 total

rm *.log *.nosex



###############################
## 2. Update sex information ##
###############################

plink --bfile infants --update-sex /data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK/infants_sex --make-bed --out tmp       ## Sex info from LORIS
plink --bfile tmp --make-bed --out infants

rm tmp.*
rm *.hh *.log



###############################################
## 3. Add father and mother's ID for infants ##
###############################################

Not needed here as done already


## Protect the files. Muwahahaha!
sudo chattr +i *.fam *.bed *.bim



##############
## 4. Merge ##
##############

echo "infants" >  toCombine
echo "mothers" >> toCombine

plink --merge-list toCombine --make-bed --out combined
rm combined.{log,hh} toCombine



################################
## 5. Split by ethnicity & QC ##
################################

cut -f1 -d" " combined.fam | sort | uniq > all_FIDs
wc -l all_FIDs                                      # 1,000 family IDs

sort /data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK/Ethnicity.txt > bbb  # Ethnicity file was derived from LORRIS on 11th April 2017
join all_FIDs bbb > eth
rm all_FIDs bbb

cut -d" " -f4 eth | sort | uniq -c
# 569 Chinese
# 249 Malay
# 182 Indian

mkdir -p ../2imputePrepSanger_input


## CHINESE #
grep Chinese eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Chinese
rm toKeep tmp.*

## MALAY #
grep Malay eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Malay
rm toKeep tmp.*

## INDIAN #
grep Indian eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Indian
rm toKeep tmp.*


## Cleanup and put each ethnicity into own folder ##
cd ../2imputePrepSanger_input/
wc -l *.fam    # All 1,933 subjects survive the stringent 97% call rate. Yay!
rm *.log *.hh

cat *.bim | cut -f2 | sort | uniq > SNPs   # 643,343

mkdir -p Chinese Malay Indian
mv Chinese.* Chinese/
mv Malay.* Malay/
mv Indian.* Indian/


#################
## EXTRA files ##
#################

wget http://www.well.ox.ac.uk/~wrayner/tools/1000GP_Phase3_combined.legend.gz
sudo chattr +i 1000GP_Phase3_combined.legend.gz

wget http://www.well.ox.ac.uk/~wrayner/strand/PsychArray-B-b37-strand.zip

OmniPrefix=/data/genotypes/GUSTO_SPRESTO_Omni1M/2imputePrepSanger_input/VARDATA/HumanOmniExpressExome-8v1-2_A-b37

cat $OmniPrefix.strand   PsychArray-B-b37.strand   | sort | uniq > combined.strand
cat $OmniPrefix.multiple PsychArray-B-b37.multiple | sort | uniq > combined.multiple
rm PsychArray-B-b37.{strand,multiple,miss}
