before_CombArrays=/data/genotypes/GUSTO_Omni1M.PsychArray/3imputePrepSanger_output/Chinese/Chinese_afterQC-updatedChr.vcf.gz
before_OmniArray=/data/genotypes/GUSTO_SPRESTO_Omni1M/3imputePrepSanger_output/Chinese/Chinese_afterQC-updatedChr.vcf.gz
after_OmniArray=/data/genotypes/GUSTO_SPRESTO_Omni1M/8forGWAS/c.forMerge.vcf.gz 

zcat $before_CombArrays | grep -v "#" | cut -f1-5 > ChineseSNPs_CombArrays
zcat $before_OmniArray  | grep -v "#" | cut -f1-5 > ChineseSNPs_OmniArray

diff ChineseSNPs_CombArrays ChineseSNPs_OmniArray > SNPdifferences
grep "< " SNPdifferences | wc -l                                         #  19,375  exclusively in combined array
grep "> " SNPdifferences | wc -l                                         #   3,340  exclusively in Omni 1M
cat ChineseSNPs_CombArrays ChineseSNPs_OmniArray | sort | uniq | wc -l   # 548,458  union


## Extract the 19,375 from the combineArrays file ##
grep "< " SNPdifferences | sed 's/< //' | awk '{print $1, $2-1, $2, $3}' | sed 's/ /\t/g' > missed.bed
rm ChineseSNPs_CombArrays ChineseSNPs_OmniArray SNPdifferences

bcftools view         -R missed.bed $before_CombArrays | grep -v "##" | sed 's/#//g' > Chinese.before.txt
bcftools view -m2 -M2 -R missed.bed $after_OmniArray   | grep -v "##" | sed 's/#//g' > Chinese.after.txt


## Next is to use the compare.R script ##
