#!/bin/bash

echo "ImputePrepSanger version 1.1: This is a script to create vcf files for imputation on the Sanger servers"

DATASTEM=$1
STRANDFILE=$2
MIND=$3
GENO=$4
MAF=$5
HWE=$6
VARDATA=$7
FIXDATA=$8
OUTPUT=$9
REMOVE_MULTI=${10}
POP=${11}
POPtol=${12}

PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}


WD2=/data/genotypes/GUSTO_Omni1M.PsychArray/2imputePrepSanger_input/


# Update build and strand
echo "== Run update_build.sh =="
./update_build.sh $WD2/$DATASTEM/$DATASTEM $VARDATA $STRANDFILE $OUTPUT/$DATASTEM"_afterQC" $OUTPUT $REMOVE_MULTI | tee -a $OUTPUT/"resultsScreen.txt"
if [ "$?" != "0" ]; then
  error_exit "Error while updating the build and strand."
fi 


# QC steps
echo "== Skipping the QC step as data has been QC-ed externally taking the family structure into account =="
 

# Also need the .bim and (from the plink --freq command) .frq files.
echo "== Run plink for creating frequency files =="
plink --bfile $OUTPUT/$DATASTEM"_afterQC" --freq --out $OUTPUT/$DATASTEM"_afterQC_freq" | tee -a $OUTPUT/"resultsScreen.txt"
if [ "$?" != "0" ]; then
  error_exit "Error with PLINK when getting the frequency."
fi


echo "== Run HRC-1000G-check-bim_v4.2.7.pl =="
perl HRC-1000G-check-bim_v4.2.7_edit.pl -b $OUTPUT/$DATASTEM"_afterQC.bim" -f $OUTPUT/$DATASTEM"_afterQC_freq.frq" -r $FIXDATA/"1000GP_Phase3_combined.legend" -g -p $POP -t $POPtol | tee -a $OUTPUT/"resultsScreen.txt"  
if [ "$?" != "0" ]; then
  error_exit "Error while running the perl script."
fi

## Now let's create the vcf files
chmod u+x Run-plink.sh 
./Run-plink.sh | tee -a $OUTPUT/"resultsScreen.txt"
if [ "$?" != "0" ]; then
  error_exit "Error while running the PLINK bash file following the perl script."
fi


echo "== Run bcftools =="
bcftools annotate -Oz --rename-chrs $FIXDATA/"ucsc2ensembl.txt" $OUTPUT/$DATASTEM"_afterQC-updated_vcf.vcf.gz" > $OUTPUT/$DATASTEM"_afterQC-updatedChr.vcf.gz"
if [ "$?" != "0" ]; then
  error_exit "Error while renaming the chromosome."
fi


bcftools norm --check-ref e -f $FIXDATA/"human_g1k_v37.fasta" $OUTPUT/$DATASTEM"_afterQC-updatedChr.vcf.gz" -o $OUTPUT/$DATASTEM"_checkRef" | tee -a $OUTPUT/"resultsScreen.txt"
if [ "$?" != "0" ]; then
  error_exit "Error while checking that the REF allele matches with GRCh37 reference."
fi

bcftools index $OUTPUT/$DATASTEM"_afterQC-updatedChr.vcf.gz" | tee -a $OUTPUT/"resultsScreen.txt"
if [ "$?" != "0" ]; then
  error_exit "Error while indexing the vcf file."
fi

echo "== Run reportRedaction =="
./reportRedaction.sh $OUTPUT $DATASTEM
if [ "$?" != "0" ]; then
  error_exit "Error while writing the report."
fi

cp -r $OUTPUT "${OUTPUT}_FullOutput"
mv "Run-plink.sh" "${OUTPUT}_FullOutput"
shopt -s extglob
cd $OUTPUT
rm -rf !("${DATASTEM}_afterQC-updatedChr.vcf.gz"|FinalReport.txt|resultsScreen.txt|"${DATASTEM}_afterQC-updatedChr.vcf.gz.csi")
cd .. 

echo "The pipeline was run successfully."




exit

## Execution ##


WD2=/data/genotypes/GUSTO_Omni1M.PsychArray/2imputePrepSanger_input
WD3=/data/genotypes/GUSTO_Omni1M.PsychArray/3imputePrepSanger_output
FIXDATA=/data/genotypes/GUSTO_SPRESTO_Omni1M/2imputePrepSanger_input/FIXDATA

./imputePrep_script_edit.sh Chinese combined 0.05 0.1 1e-6 1e-6 $WD2/VARDATA $FIXDATA $WD3/Chinese TRUE EAS 0.2
./imputePrep_script_edit.sh Malay   combined 0.05 0.1 1e-6 1e-6 $WD2/VARDATA $FIXDATA $WD3/Malay   TRUE EAS 0.3  # relaxed tol for Malays
./imputePrep_script_edit.sh Indian  combined 0.05 0.1 1e-6 1e-6 $WD2/VARDATA $FIXDATA $WD3/Indian  TRUE SAS 0.2

