[Overview](../README.md)

## Step 2. Align genotypes against reference panel (2imputePrepSanger) ##

---

#### Objectives ####

Scripts in this folder aligns the genotype data to the reference panel. Briefly, it includes the following checks:

* Updates build and strand and removes variants that map to multiple locations with high confidence
* The HRC perl script
  * Flips the strand, updates id, forces alleles to match the build file
  * Removes variants with duplicate ids
  * Removes any variants that are discordant with the reference panel in terms of: allele codings, SNPs with Freq that differs more than $POPtol from the reference population
* Converts from PLINK to VCF format and indexes
* checks the reference allele matches the the GRCh37 reference



---


#### Acknowledgements ####

These scripts are from [Marie Forest's imputePrepSanger git repository](https://github.com/eauforest/imputePrepSanger) which in turn relies on [Will Rayner's stand file and Perl script](http://www.well.ox.ac.uk/~wrayner/tools/). Our minor modifications to account for the following:

* From creating a binary file from ASCII (Line 33) to copying the binary files from previous step (line 35)
get* From using the HRC file as reference panel (Line 61) to use specific superpopulation from the 1000G panel (Line 65)
* The script now accepts two additional parameters: $POP (superpopulation to extract from the 1000G panel) and $POPtol (tolerance level for deviation from allele frequency) (Lines 15-16)

---

#### Choice of reference panel for check and imputation #####

The HRC panel contains 32,470 samples which is primarily European-descent but does include the 1000G panels. However, Xueling Sim from NUS advised us that the 1000G panel included in the HRC panel lost quite a number of variants due to data harmonization with other cohorts in the HRC. Thus we used the 1000G panels directly.

There is no reference panel for Malays in the 1000G and Xueling recommended using the EAS (i.e. Chinese, Japanese, Vietnamese) with a more lenient tolerance level. See Figure 1A of [Teo et al (Genome Research, 2009)](http://genome.cshlp.org/content/early/2009/08/20/gr.095000.109.full.pdf+html) that shows the Malays are close to Chinese genetically.


Ethnicity |  # SNPs to start | reference POP | POPtol | # SNPs for Phasing
----------|-------------~----|---------------|--------|-------------------
Chinese   |          542,329 |           EAS |   0.20 |  529,083
Malay     |          558,994 |           EAS |   0.30 |  545,207
Indian    |          594,373 |           SAS |   0.20 |  579,742

---

#### Number of SNPs for phasing and imputation  ####

   Chr      |	    Chinese     |      Malay	 |	Indian
------------|-------------------|----------------|--------------
    1	    |	     42,420	|	43,779	 |	46,776
    2	    |	     41,853	|	43,281	 |	46,074
    3	    |	     34,774	|	35,893	 |	38,321
    4	    |	     30,293	|	31,183	 |	33,169
    5	    |	     30,872	|	31,813	 |	33,872
    6	    |	     35,337	|	36,293	 |	38,657
    7	    |	     27,996	|	28,909	 |	30,633
    8	    |	     27,714	|	28,561	 |	30,428
    9	    |	     24,817	|	25,602	 |	27,086
    10	    |	     28,492	|	29,363	 |	31,066
    11	    |	     27,016	|	27,687	 |	29,242
    12	    |	     26,097	|	26,897	 |	28,600
    13	    |	     20,074	|	20,596	 |	22,071
    14	    |	     17,278	|	17,704	 |	18,787
    15	    |	     16,258	|	16,727	 |	17,588
    16	    |	     16,594	|	17,143	 |	18,165
    17	    |	     14,566	|	14,973	 |	15,892
    18	    |	     15,723	|	16,253	 |	17,353
    19	    |	     10,840	|	11,134	 |	11,738
    20	    |	     13,351	|	13,818	 |	14,713
    21	    |	      7,802	|	 8,038	 |	 8,402
    22	    |	      7,416	| 	 7,626   |	 8,207
    X	    |	     11,500	|	11,934   |	12,902
 **Total**  |	  **529,083**	|    **545,207** |    **579,742**
