#!/bin/bash

Ethnicity=$1
CHR=$2


VCF=/data/genotypes/GUSTO_SPRESTO_Omni1M/6imputeSanger_output_processed/$Ethnicity"_chr"$CHR.vcf.gz
PREFIX=/data/genotypes/GUSTO_SPRESTO_Omni1M/7forPRS/$Ethnicity"_chr"$CHR


echo -e `date +%T\ %F` $Ethnicity $CHR started >> $PREFIX.log

## Filter for non-monomorphic biallelic SNPs that are imputed with high confidence and have an rsid
bcftools view -i 'INFO > 0.8 && MAF > 0 && ID!="."' -m2 -M2 -v snps --threads 16 $VCF > $PREFIX.vcf
echo -e `date +%T\ %F` Finished filtering >> $PREFIX.log

## Remove SNPs with rsids that map to multiple ambigiously
grep -v "#" $PREFIX.vcf | cut -f3 | sort | uniq -d > $PREFIX.dups
fgrep -w -v --file=$PREFIX.dups $PREFIX.vcf > $PREFIX.tmp
mv $PREFIX.tmp $PREFIX.vcf
echo -e `date +%T\ %F` Removed `cat $PREFIX.dups | wc -l` duplicate snps >> $PREFIX.log

## Restrict to just the dosage information
bcftools annotate -x ^FORMAT/DS $PREFIX.vcf > $PREFIX.dosages.vcf

exit



#################################
## Create and execute the jobs ##
#################################

cd $HOME/gusto-imputation/6prepare_for_PRS

> jobs

for Ethnicity in Chinese Malay Indian; do
    for CHR in {1..22} X; do
	echo ./subset_for_PRS.sh $Ethnicity $CHR >> jobs
    done
done

cat jobs | xjobs -j 20



######################
## Combine the VCFs ##
######################

cd /data/genotypes/GUSTO/imputed_forPRS/

bcftools concat --threads 25 -Oz Chinese_1.vcf Chinese_2.vcf Chinese_3.vcf Chinese_4.vcf Chinese_5.vcf Chinese_6.vcf Chinese_7.vcf Chinese_8.vcf Chinese_9.vcf Chinese_10.vcf Chinese_11.vcf Chinese_12.vcf Chinese_13.vcf Chinese_14.vcf Chinese_15.vcf Chinese_16.vcf Chinese_17.vcf Chinese_18.vcf Chinese_19.vcf Chinese_20.vcf Chinese_21.vcf Chinese_22.vcf Chinese_X.vcf > ChineseForPRS.vcf.gz

bcftools concat --threads 25 -Oz Malay_1.vcf Malay_2.vcf Malay_3.vcf Malay_4.vcf Malay_5.vcf Malay_6.vcf Malay_7.vcf Malay_8.vcf Malay_9.vcf Malay_10.vcf Malay_11.vcf Malay_12.vcf Malay_13.vcf Malay_14.vcf Malay_15.vcf Malay_16.vcf Malay_17.vcf Malay_18.vcf Malay_19.vcf Malay_20.vcf Malay_21.vcf Malay_22.vcf Malay_X.vcf > MalayForPRS.vcf.gz

bcftools concat --threads 25 -Oz Indian_1.vcf Indian_2.vcf Indian_3.vcf Indian_4.vcf Indian_5.vcf Indian_6.vcf Indian_7.vcf Indian_8.vcf Indian_9.vcf Indian_10.vcf Indian_11.vcf Indian_12.vcf Indian_13.vcf Indian_14.vcf Indian_15.vcf Indian_16.vcf Indian_17.vcf Indian_18.vcf Indian_19.vcf Indian_20.vcf Indian_21.vcf Indian_22.vcf Indian_X.vcf > IndianForPRS.vcf.gz

# 10min for Indian to 20min for Chinese



#########################
## Combine the dosages ##
#########################

bcftools concat --threads 25 -Oz Chinese_1.dosages.vcf Chinese_2.dosages.vcf Chinese_3.dosages.vcf Chinese_4.dosages.vcf Chinese_5.dosages.vcf Chinese_6.dosages.vcf Chinese_7.dosages.vcf Chinese_8.dosages.vcf Chinese_9.dosages.vcf Chinese_10.dosages.vcf Chinese_11.dosages.vcf Chinese_12.dosages.vcf Chinese_13.dosages.vcf Chinese_14.dosages.vcf Chinese_15.dosages.vcf Chinese_16.dosages.vcf Chinese_17.dosages.vcf Chinese_18.dosages.vcf Chinese_19.dosages.vcf Chinese_20.dosages.vcf Chinese_21.dosages.vcf Chinese_22.dosages.vcf Chinese_X.dosages.vcf > ChineseForPRS.dosages.vcf.gz

bcftools concat --threads 25 -Oz Malay_1.dosages.vcf Malay_2.dosages.vcf Malay_3.dosages.vcf Malay_4.dosages.vcf Malay_5.dosages.vcf Malay_6.dosages.vcf Malay_7.dosages.vcf Malay_8.dosages.vcf Malay_9.dosages.vcf Malay_10.dosages.vcf Malay_11.dosages.vcf Malay_12.dosages.vcf Malay_13.dosages.vcf Malay_14.dosages.vcf Malay_15.dosages.vcf Malay_16.dosages.vcf Malay_17.dosages.vcf Malay_18.dosages.vcf Malay_19.dosages.vcf Malay_20.dosages.vcf Malay_21.dosages.vcf Malay_22.dosages.vcf Malay_X.dosages.vcf > MalayForPRS.dosages.vcf.gz

bcftools concat --threads 25 -Oz Indian_1.dosages.vcf Indian_2.dosages.vcf Indian_3.dosages.vcf Indian_4.dosages.vcf Indian_5.dosages.vcf Indian_6.dosages.vcf Indian_7.dosages.vcf Indian_8.dosages.vcf Indian_9.dosages.vcf Indian_10.dosages.vcf Indian_11.dosages.vcf Indian_12.dosages.vcf Indian_13.dosages.vcf Indian_14.dosages.vcf Indian_15.dosages.vcf Indian_16.dosages.vcf Indian_17.dosages.vcf Indian_18.dosages.vcf Indian_19.dosages.vcf Indian_20.dosages.vcf Indian_21.dosages.vcf Indian_22.dosages.vcf Indian_X.dosages.vcf > IndianForPRS.dosages.vcf.gz

# 2min for Indian to 5min for Chinese


## Cleanup ##
mkdir logs_dups
mv *.log *.dups logs_dups/
# rm *.vcf
