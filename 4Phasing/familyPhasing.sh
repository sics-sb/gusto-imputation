#!/bin/bash

Ethnicity=$1
Chr=$2
if [ $Chr == "X" ]; then plink_chr=23; else plink_chr=$Chr; fi

stem=$(mktemp)
cd /home/adai/gusto-imputation/3Phasing/


######################
## Extract the data ##
######################

plink --vcf /data/genotypes/GUSTO_SPRESTO_Omni1M/3imputePrepSanger_output/$Ethnicity/$Ethnicity"_afterQC-updatedChr.vcf.gz" --chr $plink_chr --make-bed --out $stem


#####################################
## check mother-father-child trios ##
#####################################

if [ $Chr == "X" ]; then
    shapeit -check -B $stem --output-log "logs_mendel/$Ethnicity"_chr$Chr.checks --chrX
else
    shapeit -check -B $stem --output-log "logs_mendel/$Ethnicity"_chr$Chr.checks
fi


#######################################
## Phase (with duohmm for autosomes) ##
#######################################

outFile=/data/genotypes/GUSTO_SPRESTO_Omni1M/4imputeSanger_input_phasedData/$Ethnicity"_chr"$Chr
if [ $Chr == "X" ]; then
    shapeit -T 8 -B $stem -M geneticMap_1000G/genetic_map_chrX_nonPAR_combined_b37.txt  -W 5 -O $outFile --states 200
else
    shapeit -T 8 -B $stem -M geneticMap_1000G/genetic_map_chr"$Chr"_combined_b37.txt    -W 5 -O $outFile --states 200 --duohmm
fi


####################
## Convert to VCF ##
####################

cat $outFile.haps | awk '$1=$1":"$3"_"$4"_"$5{print}' > $stem.haps
awk '{print $2,$2,$3}' $outFile.sample                > $stem.sample

bcftools convert  -Oz --hapsample2vcf $stem.haps,$stem.sample        > $outFile.vcf.gz

if [ $Chr == "X" ]; then
    bcftools annotate -Oz --rename-chrs ucsc2ensembl.txt $outFile.vcf.gz > $stem.vcf.gz
    mv $stem.vcf.gz $outFile.vcf.gz
fi


######################
## Cleanup and exit ##
######################

rm $stem.*

exit



#############
## Execute ##
#############

cd /home/adai/gusto-imputation/3Phasing/

> jobs

for Ethnicity in Chinese Malay Indian; do
    for Chr in {1..22} X; do
	echo "./familyPhasing.sh $Ethnicity $Chr" >> jobs    
    done    
done



## Misc ##
cat *.checks.ind.me | sort -nr -k5 | head
grep "SNPs with high Mendel error rate" *.log



####################################
## Concatenate all chr and check  ##
####################################

cd /data/genotypes/GUSTO_SPRESTO_Omni1M/4imputeSanger_input_phasedData


## Chinese ##
bcftools concat -Oz Chinese_chr*.vcf.gz > Chinese.vcf.gz
rm Chinese_chr*
bcftools norm --check-ref s -f human_g1k_v37.fasta Chinese.vcf.gz -o Chinese_phased.vcf
bgzip -c Chinese_phased.vcf > Chinese_phased.vcf.gz
bcftools index Chinese_phased.vcf.gz
rm Chinese.vcf.gz


## Malay ##
bcftools concat -Oz Malay_chr*.vcf.gz > Malay.vcf.gz
rm Malay_chr*
bcftools norm --check-ref s -f human_g1k_v37.fasta Malay.vcf.gz -o Malay_phased.vcf
bgzip -c Malay_phased.vcf > Malay_phased.vcf.gz
bcftools index Malay_phased.vcf.gz
rm Malay.vcf.gz


## Indian ##
bcftools concat -Oz Indian_chr*.vcf.gz > Indian.vcf.gz
rm Indian_chr*
bcftools norm --check-ref s -f human_g1k_v37.fasta Indian.vcf.gz -o Indian_phased.vcf
bgzip -c Indian_phased.vcf > Indian_phased.vcf.gz
bcftools index Indian_phased.vcf.gz
rm Indian.vcf.gz
