[Overview](../README.md)

## Step 4. Family-based phasing (4Phasing) ##

---

#### Objectives ####

The Sanger Imputation Service does not offer a pipeline that utilizes the family information during phasing. Therefore, we phase the data externally using
[SHAPEIT software with family information](https://mathgen.stats.ox.ac.uk/genetics_software/shapeit/shapeit.html#family). Steps involved (for each ethnicities) are:

1. Split the output from imputeSangerPrep folder by chromosome
2. Run shapeit -check for samples or SNPs with high Mendellian errors for each chromosome.
3. Run shapeit --duohmm for phasing autosomes. This option was omitted from the X chromosome.
4. Convert the shapeit output to VCFs (recode from chr23 to chrX)
5. Combine all VCFs into one file per ethnicity, check against GRCh37 and index

The phased files are stored in: /data/genotypes/GUSTO/PhasedData/


---

#### GUSTO family structure #####

The GUSTO cohort is mostly comprised of either trios or mother-child duos. The missingness is fathers is mostly due to refusal while missingness in mother and child is mostly due to failing sample-level QC (incorrect ethnicity in genetic PCA or incorrect parent-child inheritence pattern). Here is a summary of available family structure after including only subjects that pass sample-level QC. Note: we have six twins in the cohort that we count only once in the calculation below:

Relationship         |  Chinese |  Malay | Indian
---------------------|----------|--------|--------
Child-Mother-Father  |   419    |  182   |  115
Child-Mother         |   199    |   88   |   81
Child-Father         |    10    |    4   |    1
Mother-Father        |     7    |    5   |    2
Child only           |     3    |    2   |    3
Mother only          |    18    |    7   |   10
Father only          |     1    |    2   |    0
                     |          |        |      
SPRESTO mothers      |   356    |   79   |   48
		    
		     

---

#### Objectives ####

The Sanger Imputation Service does not offer a pipeline that utilizes the family information during phasing. Therefore, we phase the data externally using [SHAPEIT software with family information](https://mathgen.stats.ox.ac.uk/genetics_software/shapeit/shapeit.html#family). Steps involved (for each ethnicities) are:

1. Split the output from imputeSangerPrep folder by chromosome
2. Run shapeit -check for samples or SNPs with high Mendellian errors for each chromosome.
3. Run shapeit --duohmm for phasing autosomes. This option was omitted from the X chromosome.
4. Convert the shapeit output to VCFs (recode from chr23 to chrX)
5. Combine all VCFs into one file per ethnicity, check against GRCh37 and index


---

#### Next step: Upload and processing online ####

The VCFs were uploaded to the [Sanger Imputation Service](https://imputation.sanger.ac.uk/) and we choose
"1000 Genomes Phase 3" as the reference panel and
"impute with PBWT, no pre-phasing" as the pipeline.

The files were downloaded from the Sanger Imputation Service using GLOBUS software and md5sum checked.
