wget https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3.tgz
tar -xf 1000GP_Phase3.tgz --wildcards "1000GP_Phase3/genetic_map_*"

wget https://mathgen.stats.ox.ac.uk/impute/1000GP_Phase3_chrX.tgz
tar -xf 1000GP_Phase3_chrX.tgz --wildcards "genetic_map_*"

