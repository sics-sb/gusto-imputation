cd /home/adai/gusto-imputation/1prepareGenotype

GFILE=/data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK/combined


##########################
## check PI_HAT and PCA ##
##########################

## Prune
highLD=/home/adai/miniProjects/DBSR/generic/umich_highLD-regions.txt

plink --bfile $GFILE --maf 0.05 --geno 0.05 --hwe 1e-6 --make-bed --out tmp      # 549k variants after QC
plink --bfile tmp --exclude range $highLD --indep-pairwise 100 10 0.2 --out tmp  #  95k variants after pruning
plink --bfile tmp --extract tmp.prune.in --make-bed --out pruned
rm tmp.* pruned.{log,hh}


## PHI_HAT
plink --bfile pruned --genome --out pruned
cat pruned.genome | tr -s " " | cut -f3,5,6,11 -d" " > PI_HAT.txt
gzip PI_HAT.txt


## extract pruned SNP list from 1000G
cut -f2 pruned.bim | sort > toExtract  ## 220 exm-rs and 1459 exm snps will be lost

for CHR in {1..22}; do
    FILE=/data/genotypes/1000G/Phase3/integrated/ALL.chr$CHR.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
    echo "plink --vcf $FILE --extract toExtract --make-bed --out tmp$CHR" >> jobs
    echo tmp$CHR >> toCombine
done
cat jobs | xjobs -j 22                                        # < 8min
plink --merge-list toCombine --make-bed --out 1000G_Phase3
rm tmp* jobs toCombine toExtract


## set to common SNPs 90k
cut -f2 pruned.bim       | sort > tmp1
cut -f2 1000G_Phase3.bim | sort > tmp2
join tmp1 tmp2 > common

plink --bfile pruned       --extract common --make-bed --out tmp1
plink --bfile 1000G_Phase3 --extract common --make-bed --out tmp2


## Merge
plink --bfile tmp1 --bmerge tmp2 --make-bed --out combined                       # try merging which returns pruned_1000G-merge.missnp


plink --bfile tmp1 --flip combined-merge.missnp --make-bed --out tmp1flipped
plink --bfile tmp1flipped --bmerge 1000G_Phase3 --make-bed --out combined        # 7 troublesome SNP persists

plink --bfile tmp1flipped --exclude combined-merge.missnp --make-bed --out tmp3   
plink --bfile tmp2        --exclude combined-merge.missnp --make-bed --out tmp4
rm tmp1 tmp2 tmp1.* tmp2.* tmp1flipped.*

plink --bfile tmp3 --bmerge tmp4 --make-bed --out combined
rm tmp3.* tmp4.*
rm 1000G_Phase3.* pruned.*



## Prune and calculate PCA
plink --bfile combined --pca 100 --out combined
mv combined.eigenvec PCA.eigenvec.txt
mv combined.eigenval PCA.eigenval.txt
rm combined.*
