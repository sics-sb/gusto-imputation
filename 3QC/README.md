[Overview](../README.md)

## Step 3. QC (3QC) ##

---

#### Objectives ####

1) Ensure the self-reported ethnicities match genetic-derived ethnicities

2) Identify related individuals in the dataset using PI_HAT. Typical cutoff values are:

* 1     - identical
* 0.500 - first degree (e.g. parent-child)
* 0.250 - second degree
* 0.125 - third degree

Note: The GUSTO data was previously cleaned by Chen Li.



#### Ethnicity check ####

The red eclipse represents the East Asians, EAS (i.e. Chinese, Japanese, Vietnamese) from the 1000 Genomes. Subjects who self-reported to be Chinese in GUSTO and SPRESTO (first column) coincide well with this eclipse.

The blue eclipse represents the Sourh Asian, SAS (i.e. Sri Lankan Tamils, Punjabi) subjects from the 1000 Genomes.. Subjects who self-report to be Indian in GUSTO and SPRESTO (second column) have a good overlap except for a few points.

There is no reference panel for Malays in the 1000G  but Figure 1A of [Teo et al (Genome Research, 2009)](http://genome.cshlp.org/content/early/2009/08/20/gr.095000.109.full.pdf+html) shows Malays are close to Chinese genetically.

![](PCA_by_ethnicity.png)



#### Parent-offspring relationships ####

The PI_HAT values for mother-offspring and father-offspring pairs are between 0.4796 - 0.5822.


#### Consanguinity check ####

The PI_HAT values for mother-father combinations (same family) is mostly 0 with a maximum of 0.1925. One or two husband-wifes might be second or third cousins. Not a major concern for GWAS studies.


#### Within fathers #####

One pair (P010-21198 and P010-21796) appear to be identical subjects.


#### Within mothers ####

The following eight GUSTO mothers were confirmed (PI_HAT=1 and subsequently by data team) to have re-enrolled into SPRESTO: 
M010-21941/M10418; M010-20436/M10241; M010-21288/M10199; M010-21782/M10371; M010-21886/M10442; M010-22144/M10392; M010-21187/M10001; M010-04080/M10282

There are three more potentially identical samples (PI_HAT=1) that are to be resolved: M010-21707/M10479; M010-21718/M020-50079; M010-21810/M020-04307.

There are four subjects who are potentially first degree relatives (PI_HAT ~ 0.5): M10463/M10473; M10010/M10011; M020-74070/M020-74144; M010-04012/M010-21196.

And one potentially second degree relative (PI_HAT ~ 0.25): M010-20078/M010-20144

