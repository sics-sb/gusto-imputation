library(ggplot2)
library(plyr)
library(gridExtra)
library(data.table)

pcs <- fread("PCA.eigenvec.txt")
pcs <- data.frame(pcs)
pcs <- pcs[ , 1:7]
colnames(pcs) <- c("FID", "IID", paste0("PC", 1:5))


## Ethnicity for 1000G
panel1 <- read.table("/data/genotypes/1000G/Phase3/integrated/integrated_call_samples_v3.20130502.ALL.panel", header=T, row.names=1, as.is=T)[ , 1:2]
panel1$study <- "1000G"


## Ethnicity for SPRESTO
panel2 <- read.table("/data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK/Ethnicity_SPRESTO.txt", header=F, row.names=1, as.is=T)
panel2 <- panel2[ which(rownames(panel2) %in% pcs[,1]), , drop=F ]

panel2$pop       <- mapvalues( panel2[,1], from=c("Chinese", "Malay", "Indian"), to=c("CHS", "MAS", "INS") )
panel2$super_pop <- mapvalues( panel2[,1], from=c("Chinese", "Malay", "Indian"), to=c("EAS", "EAS", "SAS") )

panel2 <- panel2[ , c("pop", "super_pop")]
panel2$study <- "SPRESTO"


## Ethnicity for GUSTO
panel3 <- read.table("/data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK/Ethnicity_GUSTO.txt", header=F, row.names=1, as.is=T)
panel3 <- panel3[ which(rownames(panel3) %in% pcs[ ,1]), , drop=F ]

panel3$pop       <- mapvalues( panel3[,1], from=c("Chinese", "Malay", "Indian"), to=c("CHS", "MAS", "INS") )
panel3$super_pop <- mapvalues( panel3[,1], from=c("Chinese", "Malay", "Indian"), to=c("EAS", "EAS", "SAS") )

panel3 <- panel3[ , c("pop", "super_pop")]
panel3$study <- "GUSTO"


## Merge with PCs
panel <- rbind(panel1, panel2, panel3)
rm(panel1, panel2, panel3)

comb <- merge(pcs, panel, by.x=1, by.y=0)
comb$pop2 <- paste0(comb$study, "-", comb$pop)
comb$pop2 <- gsub("1000G-", "", comb$pop2)

comb$pop3 <- as.character(comb$pop2)
w <- which(comb$study=="1000G")
comb[w, "pop3"] <- paste0("1000G-", comb[w, "super_pop"])
rm(pcs, panel)

tb <- with(comb, table(study, pop, exclude=NULL))
tb[ which(tb==0) ] <- ""
tb
rm(tb)

write.csv(comb, file="GUSTO_SPRESTO_PCs.tsv", quote=F, row.names=F)


##########
## Plot ##
##########

comb <- read.csv("GUSTO_SPRESTO_PCs.tsv")
comb$PC1 <- -10000*comb$PC1
comb$PC2 <- -10000*comb$PC2
comb <- comb[ order(comb$PC1), ]

Asians <- subset(comb, super_pop %in% c("EAS", "SAS"))
Asians <- droplevels(Asians)
table(Asians$pop3)
#          1000G   GUSTO  SPRESTO
# Chinese    504   1,712      356
# Malay        0     751       79
# Indian     489     526       48
# --------------------------------
#                  2,989      483


pdf("PCAplots.pdf", width=16, height=10)
ggplot( subset(comb, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point() + ggtitle("1000G only") + theme_bw() +
  xlab("Principal component 1") + ylab("Principal component 2")

ggplot( data=subset(Asians, study=="1000G"), 
        aes(x=PC1, y=PC2, col=pop) ) + geom_point() + theme_bw() +
  xlab("Principal component 1") + ylab("Principal component 2")

ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  geom_point() + theme_bw() +
  xlab("Principal component 1") + ylab("Principal component 2")

ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() +
  xlab("Principal component 1") + ylab("Principal component 2")

ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + 
  xlab("Principal component 1") + ylab("Principal component 2")


g1 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="GUSTO-CHS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("GUSTO Chinese")

g2 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="GUSTO-INS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("GUSTO Indian")

g3 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="GUSTO-MAS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("GUSTO Malay")

g4 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="SPRESTO-CHS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("SPRESTO Chinese")

g5 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="SPRESTO-INS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("SPRESTO Indian")

g6 <- ggplot( data=subset(Asians, study=="1000G"), aes(x=PC1, y=PC2, col=super_pop) ) + 
  geom_point(data=subset(comb, pop3=="SPRESTO-MAS"), col="black") +
  stat_ellipse(geom="polygon", alpha=0.2, lwd=0.5, aes(fill=super_pop), type="norm") +
  theme_bw() + ggtitle("SPRESTO Malay")


tp <- theme(legend.position="none", axis.title.x=element_blank(), axis.title.y=element_blank())
grid.arrange(g1 + tp, g2 + tp, g3 + tp, 
             g4 + tp, g5 + tp, g6 + tp, 
             bottom="Principal component 1", left="Principal component 2",
             nrow=2)

ggplot( data=Asians, aes(x=PC1, y=PC2, col=pop3) ) + geom_point() + 
  stat_ellipse(geom="polygon", alpha=0.1, lwd=0.5) + theme_bw()

dev.off()

rm(g1, g2, g3, g4, g5, g6, tp, Asians, tb, w)


#################################
## Identify potential outliers ##
#################################

max(subset( comb, pop3=="1000G-SAS", PC1, drop=T ))  # -14.593
min(subset( comb, pop3=="1000G-EAS", PC1, drop=T ))  # 94.8581


## Check Chinese ##
subset( comb, pop3 %in% c("GUSTO-CHS", "SPRESTO-CHS") & PC1 < 95, c(1:4,12) )
#            FID        IID     PC1      PC2      pop3
# 2835 020-66165 M020-66165 94.2305 -29.0146 GUSTO-CHS  # close to boundary so OK


## Check Indian ##
subset( comb, pop3 %in% c("GUSTO-INS", "SPRESTO-INS") & PC1 > -15, c(2:4,12) )


## Check Malay ##
subset( comb, pop3 %in% c("GUSTO-MAS", "SPRESTO-MAS") & PC1 < 0, c(1:4,12) )
#       FID        IID      PC1     PC2      pop3
# 010-22083 P010-22083 -19.1845 120.146 GUSTO-MAS

subset( comb, pop3 %in% c("GUSTO-MAS", "SPRESTO-MAS") & PC1 > 105, c(1:4,12) )
#            FID        IID     PC1      PC2        pop3
# 1560 010-21706 P010-21706 105.913 -47.5619   GUSTO-MAS
# 464  010-20398 M010-20398 106.051 -49.3707   GUSTO-MAS
# 1558 010-21706 B010-21706 106.387 -52.8160   GUSTO-MAS
# 1239 010-21360 M010-21360 106.457 -46.1885   GUSTO-MAS
# 2015 010-22147 P010-22147 107.170 -46.9090   GUSTO-MAS
# 758  010-20815 P010-20815 107.263 -49.4222   GUSTO-MAS
# 222  010-20113 M010-20113 107.387 -46.7241   GUSTO-MAS
# 3029     10050     M10050 107.480 -48.6268 SPRESTO-MAS
# 3001     10015     M10015 108.351 -51.1508 SPRESTO-MAS
# 3184     10221     M10221 109.974 -54.9167 SPRESTO-MAS
# 890  010-20962 M010-20962 110.774 -52.5015   GUSTO-MAS
# 1977 010-22119 M010-22119 111.986 -51.7685   GUSTO-MAS
# 455  010-20390 M010-20390 112.166 -53.5439   GUSTO-MAS


subset(comb, FID=="019-30106")

