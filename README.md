# Overview of the steps involved #

Here are the major steps involved in the imputation and post-processing for the GUSTO cohort. They are organized into subfolders with a self-contained README. The major steps (folder names in parenthesis) are:

[Step 1. Prepare the genotype data (1prepareGenotype)](1prepareGenotype/README.md)   
[Step 2. Align genotypes against reference panel (2imputePrepSanger)](2imputePrepSanger/README.md)    
[Step 3. Quality check on genotype data (3QC)](3QC/README.md)    
[Step 4. Family-based phasing (4Phasing)](4Phasing/README.md)   
[Step 5. Process outputs from Sanger Imputation Service (5processImputeSanger)](5processImputeSanger/README.md)   
[Step 6. Prepare files for PRS calculations (6prepare_for_PRS)](6prepare_for_PRS/README.md)   
[Step 7. Prepare files for GWAS (7prepare_for_GWAS)](7prepare_for_GWAS/README.md)

---

## About GUSTO ##

[Growing Up in Singapore Towards Healthy Outcomes (GUSTO)](http://www.gusto.sg/) is Singapore’s largest and most comprehensive birth cohort study. It is a prospective cohort of ~1,200 families that is representative of the major ethnicities in Singapore (Chinese, Malays and Indians).

Imputing genotype from a family-based and multi-ethnic cohort data presents a few additional challenges. This repository decribes our approach to solve them.

---

## Some top-level general decisions ##

* Preprocess, phase and impute data from the different ethnicities separately
* Use family-based phasing to maximise the pinformation for phasing the infant
* To only use data from the Illumina OmniExpressExome array data. See below.

The GUSTO cohort was genotyped on the Illumina OmniExpressExome array (~1 million SNPs) and the PsychArray-B (~200,000). However, the PsychArray-B did not add much new information over the OmniExpressExome array (~17k additional SNPs after QC) and was incomplete (~14% of mothers, 12% of infants and 100% of fathers were not genotyped on PschArray).

---

## Contributors ##

* Adaikalavan Ramasamy (adai@sics.a-star.edu.sg)
* Chen Li
* Neerja Karnani

With advice and support from Xueling Sim, Michelle Kee Zhi Ling, Kieran O’Donnell, Lawrence M Chen, Michael Meaney.
