[Overview](../README.md)

## Step 1. Prepare the genotype data (1prepareGenotype) ##

---

#### Objectives ####

1. Converts from ASCII version of PLINK (.ped, .map) to binary version (.bed, .bim, .fam)
2. Updates the sex information
3. Adds mother and father IDs for the child fam file
4. Combines mother, father and child genotype files and splits them by ethnicity
5. Runs QC on each ethnicity. SNP-level QC includes: MAF > 5%, call rate > 95% and HWE p-value > 1e-6 (these parameters were estimated only using only the parents).

---

#### Number of SNPs and subjects ####


Ethnicity |  # SNPs | # Samples | # Mothers (GUSTO + SPRESTO) | # Fathers | # Child (boys,girls)
----------|---------|-----------|-----------------------------|-----------|----------------------
Chinese   | 542,329 |    2,068  |                   643 + 356 |       437 |       632 (329,303)
Malay     | 558,994 |      830  |                    282 + 79 |       193 |       276 (151,125)
Indian    | 594,373 |      574  |                    208 + 48 |       118 |       200 (106, 94)
**Total** |         | **3,472** |                   **1,616** |   **748** | **1,108** (586,522)


#### Output directory ####

The resulting files are stored (by ethnicity) in the server in the 2imputePrepSanger_input/ folder.