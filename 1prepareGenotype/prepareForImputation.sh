##############################################################################################
## 1. Converts the ASCII version of PLINK (.ped, .map) to binary version (.bed, .bim, fam). ##
##    This compresses from 11GB to 736MB and faster to load in PLINK                        ##
##############################################################################################

cd /data/genotypes/GUSTO_SPRESTO_Omni1M/1genotyped_PLINK

GDIR=/home/adai/NAS/DATA/All_Omni_SNPdata_GUSTO/for_Adai/PLINK/           ## where Chen Li dumped the data for me

plink --bfile $GDIR/infant_1108  --make-bed --out infants
plink --bfile $GDIR/GUSTO_SPRESTO_mother1616 --make-bed --out mothers
plink --bfile $GDIR/father_748   --make-bed --out fathers

rm *.log *.nosex



###############################
## 2. Update sex information ##
###############################

## Update sex for fathers
awk '{$5=1}{print $0}' fathers.fam > tmp;  mv tmp fathers.fam


## Cleanup FID, IID in mothers and update sex (GUSTO FID is xxx-xxxxx and SPRESTO FID is xxxxx)
cut -d" " -f2 mothers.fam | cut -d"_" -f2 | sed 's/.Top//g' | sed 's/M//g' > motherIDs
awk '{print $1, "M"$1, 0, 0, 2, -9}' motherIDs > mothers.fam
rm motherIDs


## Cleanup FID values in infants and update sex
cut -d" " -f1  infants.fam | sed 's/-B1//' | sed 's/-B2//' > aaa
cut -d" " -f2- infants.fam > bbb
paste -d" " aaa bbb > infants.fam
rm aaa bbb

plink --bfile infants --update-sex infants_sex --make-bed --out tmp       ## Sex info from LORIS
plink --bfile tmp --make-bed --out infants

rm tmp.*
rm *.hh *.log



###############################################
## 3. Add father and mother's ID for infants ##
###############################################

awk '{$3="P"$1; $4="M"$1}{print $0}' infants.fam > tmp;  mv tmp infants.fam

## Protect the files. Muwahahaha!
sudo chattr +i *.fam *.bed *.bim infants_sex toFlip.txt



##############
## 4. Merge ##
##############

echo "infants" >  toCombine
echo "mothers" >> toCombine
echo "fathers"  >> toCombine

plink --merge-list toCombine --make-bed --out combined
rm combined.{log,hh} toCombine



################################
## 5. Split by ethnicity & QC ##
################################

cut -f1 -d" " combined.fam | sort | uniq > all_FIDs
wc -l all_FIDs                                      # 1,642 family IDs

sort Ethnicity.txt > bbb                            # Ethnicity file was derived from LORRIS on 11th April 2017
join all_FIDs bbb > eth
rm all_FIDs bbb

grep self-reported eth  | wc -l                     # Self-reported ethnicity

cut -d" " -f4 eth | sort | uniq -c
# 1013 Chinese
# 369 Malay
# 260 Indian

mkdir -p ../2imputePrepSanger_input


## CHINESE #
grep Chinese eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Chinese
rm toKeep tmp.*

## MALAY #
grep Malay eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Malay
rm toKeep tmp.*

## INDIAN #
grep Indian eth | cut -d" " -f1 > toKeep
plink --bfile combined --keep-allele-order --keep-fam toKeep --mind 0.03 --make-bed --out tmp
plink --bfile tmp --filter-founders --geno 0.05 --maf 0.05 --hwe 1e-6 --write-snplist --out tmp
plink --bfile tmp --extract tmp.snplist --keep-allele-order --make-bed --out ../2imputePrepSanger_input/Indian
rm toKeep tmp.*


## Cleanup and put each ethnicity into own folder ##
cd ../2imputePrepSanger_input/
wc -l *.fam    # All 3,472 subjects survive the stringent 97% call rate. Yay!
rm *.log *.hh

cat *.bim | cut -f2 | sort | uniq > SNPs   # 620,996

mkdir -p Chinese Malay Indian
mv Chinese.* Chinese/
mv Malay.* Malay/
mv Indian.* Indian/


#################
## EXTRA files ##
#################

wget http://www.well.ox.ac.uk/~wrayner/tools/1000GP_Phase3_combined.legend.gz
sudo chattr +i 1000GP_Phase3_combined.legend.gz

wget http://www.well.ox.ac.uk/~wrayner/strand/OmniExpressExome-8v1-1_A-b37-strand.zip
unzip OmniExpressExome-8v1-1_A-b37-strand.zip

wget http://www.well.ox.ac.uk/~wrayner/strand/HumanOmniExpressExome-8v1-2_A-b37-strand.zip
unzip HumanOmniExpressExome-8v1-2_A-b37-strand.zip


###################################################
## Identify the most compatible strand file in R ##
###################################################

length( snps <- read.delim(file="SNPs", header=F, as.is=T)[ ,1] )  # 628,782
v11 <- read.delim(file="OmniExpressExome-8v1-1_A-b37.strand", header=F, row.names=1, as.is=T)
v12 <- read.delim(file="HumanOmniExpressExome-8v1-2_A-b37.strand", header=F, row.names=1, as.is=T)

length( w1 <- intersect( snps, rownames(v11) ) )  # 627,804
length( w2 <- intersect( snps, rownames(v12) ) )  # 628,703

length( w <- intersect( w1, w2 ) )  # 627,804 overlapping
v11 <- v11[ w, ]
v12 <- v12[ w, ]
rm(w1, w2, w)

for(i in 1:5) print( table( v11[ ,i] == v12[ ,i] ) )
which(v11[ ,2] =! v12[ ,2])

w <- unique( c( which(v11[ ,2] != v12[ ,2]), which(v11[ ,3] != v12[ ,3]), which(v11[ ,4] != v12[ ,4]) ) )

v11[ w, ]
#           V2       V3       V4 V5 V6
# exm20280   1 46769511 99.17355  + AG
# rs3949847 13 48710742 98.34711  + AC
# rs727238  16 23521066 98.34711  - AG

v12[ w, ]
#           V2       V3       V4 V5 V6
# exm20280   1 16134072 99.17355  - AG
# rs3949847 13 48710742 99.17355  + AC
# rs727238  16 23521066 99.17355  - AG
