[Overview](../README.md)

## Step 5. Process outputs from Sanger Imputation Service (5processImputeSanger) ##

---

#### Raw output ####


The data was downloaded from the Sanger Imputation Service website (stored by ethnicity in /data/genotypes/GUSTO/fromImputeSanger/) and md5sum checked. Information is available ~85 million variants.

The INFO column contains the following information


| INFO Element |  Meaning                                        |
|--------------|-------------------------------------------------|
| TYPED        |  was the site was genotyped prior to imputation |
| RefPanelAF   |  Allele frequency in imputation reference panel |  
| AN           |  Total number of alleles in called genotypes    |
| AC           |  Allele count in genotypes                      |
| INFO         |  The IMPUTE2 info score                         |

The cell value for each variant and subject is in the form of GT:ADS:DS:GP (e.g. 0|0:0.05,0.05:0.1:0.9025,0.095,0.0025)

| FORMAT element |  Meaning                                 |
|------|----------------------------------------------------|
| GT   |  Genotype (most likely genotype in phased format)  |
| ADS  |  Allele dosage per haplotype                       |
| DS   |  Genotype dosage                                   |
| GP   |  Genotype posterior probabilities                  |


---


#### Objectives ####

bcftools can calculate the minor allele frequency (MAF) on the fly but I prefer to hard code as an additional value in the INFO column because this value needs to be calculated using only the parents. The addMAF_getSummary.sh script does this and also produces a summary statistics for each variant in the file.

The output from these were then used for preparation of the files for PRS and also for GWAS.
