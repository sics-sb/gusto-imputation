#!/bin/bash

Ethnicity=$1
CHR=$2

VCF=/data/genotypes/GUSTO_SPRESTO_Omni1M/5imputeSanger_output/$Ethnicity/$CHR.vcf.gz
PREFIX=/data/genotypes/GUSTO_SPRESTO_Omni1M/6imputeSanger_output_processed/$Ethnicity"_chr"$CHR


## Extract parents and calculate MAF and HWE ##
bcftools query --list-samples $VCF | grep -v B > $PREFIX.keep
bcftools view --threads 8 --samples-file $PREFIX.keep $VCF | bcftools +fill-tags -- -t MAF,HWE  > $PREFIX.parents.vcf
bgzip -c $PREFIX.parents.vcf > $PREFIX.parents.vcf.gz
tabix $PREFIX.parents.vcf.gz
rm $PREFIX.keep


## Print out the summary info setting very small INFO/INFO values to zero ##
bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT\t%INFO/INFO\t%INFO/MAF\t%RefPanelAF\t%TYPED\n' $PREFIX.parents.vcf | awk '{OFS="\t"; if($6 < 0.01){$6=0}; print }' > $PREFIX.summary


## Extract the infants ##
bcftools query --list-samples $VCF | grep B  > $PREFIX.keep
bcftools view --threads 8 --samples-file $PREFIX.keep $VCF > $PREFIX.infants.vcf
bgzip -c $PREFIX.infants.vcf > $PREFIX.infants.vcf.gz
tabix $PREFIX.infants.vcf.gz
rm $PREFIX.keep


## Merge, cleanup and exit ##
bcftools merge --threads 8 $PREFIX.parents.vcf.gz $PREFIX.infants.vcf.gz > $PREFIX.vcf
bgzip -c $PREFIX.vcf > $PREFIX.vcf.gz
rm $PREFIX.parents.* $PREFIX.infants.* $PREFIX.vcf
exit



#################################
## Create and execute the jobs ##
#################################

cd $HOME/gusto-imputation/5processImputeSanger/

> jobs

for Ethnicity in Chinese Malay Indian; do
    for CHR in {1..22} X; do
	echo ./addMAF_getSummary.sh $Ethnicity $CHR >> jobs
    done
done

cat jobs | xjobs -j 20
