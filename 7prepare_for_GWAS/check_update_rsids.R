library(data.table)

## rs IDs from genotyped file ##
bim <- fread("/data/genotypes/GUSTO/PLINK_binary/mother_1108.bim")
bim[ , ori.chr := gsub("23", "X", V1)]
bim[ , chrpos := paste0(ori.chr, "_", V4) ]
setnames(bim, "V2", "ori.rsID")
bim <- bim[ , c("chrpos", "ori.chr", "ori.rsID"), with=F ]


## rs IDs from imputeSanger on genotyped SNPs ##
sel <- fread("zcat full_summary.txt.gz | awk '$6 !=\"...\" && $13==1 {print $1, $2, $3, $4, $5}'")
sel[ , chrpos := paste0(V1, "_", V2) ]
sel[ , table(nchar(V4))] # all are SNPs
sel[ , table(nchar(V5))] # all are SNPs
setnames(sel, "V1", "chr")
setnames(sel, "V3", "new.rsID")
sel <- sel[ , c("chrpos", "chr", "new.rsID"), with=F ]


## Merge ##
length( intersect( sel$chrpos, bim$chrpos ) )  # 611,744 overlap
setdiff( sel$chrpos, bim$chrpos )              # Only one genotyped snp excluded (chr5:102989429)

comb <- data.table:::merge.data.table(sel, bim, by="chrpos", all.x=T)
comb[ new.rsID == ".", "new.rsID" ] <- NA              # 28,143 no rsid
comb[ , ori.rsID2 := gsub("exm-rs", "rs", ori.rsID) ]  #  2,923 with exm-rs
comb[ grep("exm", ori.rsID2), "ori.rsID2" ] <- NA       # 12,673 no rsid

with( comb, table( is.na(new.rsID), is.na(ori.rsID2)) )
# 574,507 found in both. 720 are different. 99.9% are same so yay!
#     655 missing in both
#   9,095 was previously missing. Now filled in. Yay!
#  27,488 was previously available but now missing. 50% of these are from X chromosome. Urgh ... need to fill in

tmp <- subset(comb, !is.na(new.rsID) & !is.na(ori.rsID2))
tmp[ new.rsID != ori.rsID2, ]

tmp <- subset(comb, !is.na(new.rsID) & is.na(ori.rsID2))

tmp <- subset(comb, is.na(new.rsID) & is.na(ori.rsID2))

tobefilled <- subset(comb, is.na(new.rsID) & !is.na(ori.rsID2))
tobefilled[ , table(ori.rsID==ori.rsID2)]
tobefilled[ ori.rsID!=ori.rsID2, ] 

sort( round( 100*tobefilled[ , table(chr)][ c(1:22, "X") ] / comb[ , table(chr)][ c(1:22, "X") ], 1 ) )  # disproportionately higher loss in X chromosome (23%) and chr 6 (12%) vs other chromosomes (2 - 6%)
tobefilled




## Check against build 150
fwrite( comb[ , list(chrpos)], file="/data/genotypes/dbSNP_b150/toGrep" )
cd /data/genotypes/dbSNP_b150/
fgrep -w --file=toGrep rsid_dbSNP_b150.snps.txt > rsid_dbSNP_b150.snps_genotyped.txt

b150 <- fread("/data/genotypes/dbSNP_b150/rsid_dbSNP_b150.snps_genotyped.txt", header=F)
colnames(b150) <- c("chrpos", "b150.rsID")

comb2 <- data.table:::merge.data.table(comb, b150, by="chrpos", all.x=T)

with( comb2, table( is.na(b150.rsID), is.na(new.rsID)) )
tmp <- subset(comb2, is.na(b150.rsID) & !is.na(new.rsID))


with( comb2, table( is.na(b150.rsID), is.na(ori.rsID2)) )

tmp <- subset(comb2, !is.na(b150.rsID) & !is.na(new.rsID))
tmp[ b150.rsID != new.rsID, ]


tmp <- subset(comb2, !is.na(b150.rsID) & !is.na(ori.rsID2))
tmp[ b150.rsID != ori.rsID2, ]

tmp <- subset(comb2, !is.na(b150.rsID) & !is.na(new.rsID) & !is.na(ori.rsID2))
tmp[ -which(b150.rsID == new.rsID & new.rsID == ori.rsID2) ] # 509 

comb2[ is.na(new.rsID) & is.na(ori.rsID2) & !is.na(b150.rsID) ]

comb2[ is.na(b150.rsID), .N, by=chr]
