#!/bin/bash

Ethnicity=$1
CHR=$2

VCF=/data/genotypes/GUSTO_SPRESTO_Omni1M/6imputeSanger_output_processed/$Ethnicity"_chr"$CHR.vcf.gz  # input VCF
PREFIX=/data/genotypes/GUSTO_SPRESTO_Omni1M/8forGWAS/$Ethnicity"_chr"$CHR                            # Prefix for output files
KEEP=/home/adai/gusto-imputation/7prepare_for_GWAS/keep/chr$CHR.txt                                  # Pattern file (chr, pos, ID, ref, alt)

bcftools view -h $VCF > $PREFIX.tmp
zcat $VCF | fgrep -w --file $KEEP >> $PREFIX.tmp
sleep 15

# Previously, bcftools annotate -x ^FORMAT/DS $PREFIX.tmp > $PREFIX.vcf



#########################
## Update VCF info col ##
#########################

pre=`echo $Ethnicity | awk '{print substr(tolower($0),0,1)}'`
cohort="GUSTO and SPRESTO $Ethnicity"

bcftools view -h $PREFIX.tmp | grep -v -e "ID=INFO" -e "ID=MAF" -e "ID=HWE" -e "GL000" -e "bcftools" -e "#CHROM"           > $PREFIX.header
echo -e "##INFO=<ID=${pre}INFO,Number=1,Type=Float,Description=\"IMPUTE2 info score in $cohort\">"                        >> $PREFIX.header
echo -e "##INFO=<ID=${pre}MAF,Number=A,Type=Float,Description=\"Minor Allele frequency in $cohort parents\">"             >> $PREFIX.header
echo -e "##INFO=<ID=${pre}HWE,Number=A,Type=Float,Description=\"HWE test in $cohort parents\">"                           >> $PREFIX.header
bcftools view -h $PREFIX.tmp | grep "#CHROM"                                                                              >> $PREFIX.header

grep -v "#" $PREFIX.tmp | sed "s/INFO=/${pre}INFO=/g; s/MAF=/${pre}MAF=/g; s/HWE/${pre}HWE/g" > $PREFIX.bottom

cat $PREFIX.header $PREFIX.bottom > $PREFIX.comb
bcftools annotate --threads 8 -x INFO/TYPED,INFO/AN,INFO/AC,INFO/RefPanelAF $PREFIX.comb | grep -v bcftools > $PREFIX.vcf
bgzip $PREFIX.vcf
tabix $PREFIX.vcf.gz

rm $PREFIX.{header,bottom,comb,tmp}

exit



#################################
## Create and execute the jobs ##
################################# 

cd $HOME/gusto-imputation/7prepare_for_GWAS/

> jobs

for CHR in {22..1} X; do
    for Ethnicity in Chinese Malay Indian; do
        echo ./subset_VCF.sh $Ethnicity $CHR >> jobs
    done
done

cat jobs | xjobs -j 36

