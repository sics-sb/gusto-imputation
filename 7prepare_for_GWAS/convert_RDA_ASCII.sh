cd /data/genotypes/GUSTO_SPRESTO_Omni1M/9rda_ascii/


VCF=/data/genotypes/GUSTO_SPRESTO_Omni1M/8forGWAS/GUSTO_SPRESTO.vcf.gz
# time zcat $VCF | grep -v "#" | wc -l  # 8,001,478 and 30min


## Prepare the INFO file
VCF=/data/genotypes/GUSTO_SPRESTO_Omni1M/8forGWAS/GUSTO_SPRESTO.vcf.gz
echo 'CHROM POS REF ALT ID cHWE mHWE iHWE cINFO mINFO iINFO cMAF mMAF iMAF' > INFO
time bcftools query -f '%CHROM %POS %REF %ALT %ID %cHWE %mHWE %iHWE %cINFO %mINFO %iINFO %cMAF %mMAF %iMAF\n' $VCF >> INFO  # 11min

cut -f1-5 INFO | sort > ID5.sorted
cut -f1,2,5 -d" " ID5.sorted | uniq -d | wc -l     # 18,109. Possibly multi-allelic
cut -f1-2 -d" " ID5.sorted | uniq -d | wc -l       # 40,791 positions have multiple variants
cut -f1-4 -d" " ID5.sorted | uniq -d | wc -l
cut -f1-5 -d" " ID5.sorted | uniq -d | wc -l       # 15 variants are duplicated. Strange but all are indels
cut -f1-5 -d" " ID5.sorted | uniq -d > duplicated
fgrep --file=duplicated ID5.sorted  | wc -l        # each of the 15 have been duplicated twice


## Troublesome 15 variants (all are indels)
rs11320697
rs113440442
rs138950042
rs139449591
rs139660975
rs144672882
rs146991281
rs148858161
rs151047521
rs200115073
rs34340907
rs34791496
rs375458669
rs61156644
rs71693912



## extract
bcftools query -l $VCF > sampleNames
time bcftools query -f '%CHROM %POS %REF %ALT %ID[\t%DS]\n' $VCF | sed 's/ /:/g' > dosages_noHeader.txt   # 2 hour 40min

cut -f1-5 -d" " ID5.sorted | uniq -d | sed 's/ /:/g' > duplicated
sed 's/ /:/g' duplicated > duplicated2


## Remove the troublesome 15
fgrep --file=duplicated INFO > INFO2

fgrep -v --file=duplicated2 dosages_noHeader.txt > dosages_noHeader.txt2

cut -f1 dosages_noHeader.txt2 > rownamesD
cut -f1-5 INFO2 -d" " | sed 's/ /:/g' | grep -v CHROM > rownamesI
diff rownamesD rownamesI                                            # no difference

mv INFO2 INFO
mv dosages_noHeader.txt2 dosages_noHeader.txt

rm duplicated duplicated2 ID5.sorted rownamesD rownamesI
