#!/bin/bash

CHR=$1

cd /data/genotypes/GUSTO_SPRESTO_Omni1M/8forGWAS


##############################
## Merge across ethnicities ##
##############################

bcftools merge --threads 10 Chinese_chr$CHR.vcf.gz Malay_chr$CHR.vcf.gz Indian_chr$CHR.vcf.gz > chr$CHR.vcf
bgzip chr$CHR.vcf
tabix chr$CHR.vcf.gz

exit



#################################
## Create and execute the jobs ##
################################# 

cd $HOME/gusto-imputation/7prepare_for_GWAS/

> jobs2

for CHR in {1..22} X; do
    echo ./merge_subsets.sh $CHR >> jobs2
done

cat jobs | xjobs -j 23


#######################
## Protect the files ##
#######################

echo -e "ID_1 ID_2 missing\n0 0 0" > samples
bcftools query --list-samples chr22.vcf.gz | awk '{print $1,$1,0}' >> samples

rm Chinese_* Malay_* Indian_*



#########################
## Merge into one file ##
#########################

time bcftools concat --threads 40 chr1.vcf.gz chr2.vcf.gz chr3.vcf.gz chr4.vcf.gz chr5.vcf.gz chr6.vcf.gz chr7.vcf.gz chr8.vcf.gz chr9.vcf.gz chr10.vcf.gz chr11.vcf.gz chr12.vcf.gz chr13.vcf.gz chr14.vcf.gz chr15.vcf.gz chr16.vcf.gz chr17.vcf.gz chr18.vcf.gz chr19.vcf.gz chr20.vcf.gz chr21.vcf.gz chr22.vcf.gz chrX.vcf.gz > GUSTO_SPRESTO.vcf


bgzip GUSTO_SPRESTO.vcf                 # 2 hours and 15min
tabix GUSTO_SPRESTO.vcf.gz              # 17 min

sudo chattr +i GUSTO_*



strip dosages only




