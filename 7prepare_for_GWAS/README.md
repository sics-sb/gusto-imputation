[Overview](../README.md)

## Step 7. Prepare files for GWAS (7prepare_for_GWAS) ##

---

#### Variant selection ####

1. For each ethnicity, select variants that pass both of these criteria
      * Imputation score, INFO > 0.50
      * Minor allele frequency, MAF > 5%
2. Take the union of all passing variants across three ethnicities

A total of 6,458,661 variants pass in Chinese; 6,672,629 variants pass in Malays and 7,216,007 variants pass in Indians. The venn diagram for overlap is given below.
![](venn_snps_ethnicities.png)


The union is 8,001,465 variants which represents less than 10% of the variants from the Sanger Imputation Service.


#### Number of variants retained by chromosome ####

CHROM	  |   # Imputed variants	 |   # Variants kept
----------|------------------------------|-------------------
1	  |	6,500,358		 |	    602,218
2	  |	7,117,614		 |	    640,889
3	  |	5,862,629		 |	    559,717
4	  |	5,763,673		 |	    572,857
5	  |	5,293,915		 |	    490,878
6	  |	5,051,641		 |	    540,716
7	  |	4,741,583		 |	    460,547
8	  |	4,622,575		 |	    420,027
9	  |	3,579,889		 |	    332,821
10	  |	4,013,458		 |	    401,396
11	  |	4,067,158		 |	    382,870
12	  |	3,889,061		 |	    380,284
13	  |	2,872,968		 |	    287,087
14	  |	2,669,300		 |	    254,989
15	  |	2,437,477		 |	    225,634
16	  |	2,713,871		 |	    235,063
17	  |	2,341,782		 |	    207,348
18	  |	2,279,214		 |	    219,770
19	  |	1,843,199		 |	    182,420
20	  |	1,822,225		 |	    166,316
21	  |	1,112,215		 |	    106,577
22	  |	1,110,217		 |          102,892
X	  |	3,461,431		 |          228,149
**Total** |	**85,167,453**		 |      **8,001,465**



#### Converting to RDA and ASCII formats ####

The scripts convert_RDA_ASCII.sh and convert_RDA_ASCII.R are used for this.
I also removed a few rows (all corresponding to indels) that had the same values for chr, pos, alt, ref and rsid so the final number of rows are 8,001,448.
The unique identified for rownames is chr:pos:ref:alt:rsid to account for multi-allelic variants (not all variants had an rsid).





